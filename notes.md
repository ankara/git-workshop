This is a git workshop.

#DevSecOpsAnkara

## Commands to Run
* git branch -m master main
* git fetch origin
* git branch -u origin/main main
* git remote set-head origin -a

NOTE:
1. make sure after the above commands are executed, to delete/remove the "master" branch, change the default branch to "main"
2. After step one, do git push origin --delete master


Disclaimer: These were performed on our repo at codeberg
https://codeberg.org/ankara/git-workshop/

Thanks for all the questions,and to those who attended the session :)